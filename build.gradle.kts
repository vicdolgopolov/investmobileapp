buildscript {
    val kotlin_version by extra("1.9.24")
    val hilt_version by extra("2.48")

    repositories {
        google()
        mavenCentral()
    }

    dependencies {
        classpath("com.android.tools.build:gradle:8.0.2")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version")
        classpath("com.google.dagger:hilt-android-gradle-plugin:$hilt_version")
        classpath("com.google.gms:google-services:4.4.2")
    }
}

plugins {
    // ...
    id("com.google.dagger.hilt.android") version "2.48" apply false
    // Add the dependency for the Google services Gradle plugin
}


    tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}





