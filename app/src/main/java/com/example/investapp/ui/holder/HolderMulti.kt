package com.example.investapp.ui.holder

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.investapp.R
import com.example.investapp.databinding.HolderMultiValueBinding
import com.example.investapp.databinding.HolderValueBinding
import com.example.investapp.pojo.MultiValue
import com.example.investapp.pojo.OverviewValue

// Определяем класс HolderMulti, который содержит ссылку на данные мультипликатора для отображения в представлении
class HolderMulti(val view: HolderMultiValueBinding) : ViewHolder(view.root) {
    // Функция для установки данных в элементы представления
    fun set(multi: MultiValue) {
        view.name.text = multi.name
        view.value.text = multi.value
        view.text.text = multi.text
    }
}