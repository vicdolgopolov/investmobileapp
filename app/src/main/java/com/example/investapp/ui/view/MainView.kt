package com.example.investapp.ui.view

import android.util.Log
import com.example.investapp.Intents
import com.example.investapp.adapter.AdapterFav
import com.example.investapp.adapter.AdapterStock
import com.example.investapp.data.FavsDB
import com.example.investapp.databinding.ActivityMainBinding
import com.example.investapp.ui.MainActivity
import com.example.investapp.data.StocksTopData
import com.example.investapp.pojo.StockPojo
import com.example.investapp.util.Visible

class MainView(val activity: MainActivity) {

    var view: ActivityMainBinding = ActivityMainBinding.inflate(activity.layoutInflater) // Инициализируем связывание представлений
    var adapter = AdapterFav() // Создаем экземпляр адаптера для избранных акций

    init {
        activity.setContentView(view.root) // Устанавливаем корневое представление активности
        initUi()
    }

    fun initUi(){ // Функция для инициализации пользовательского интерфейса
        view.searchBtn.setOnClickListener{Intents.from(activity).toSearch()} // Устанавливаем обработчик нажатия на кнопку поиска
        view.stocks.adapter = AdapterStock(StocksTopData.getTop(activity)) // Устанавливаем адаптер для списка акций
        view.favs.adapter = adapter // Устанавливаем адаптер для списка избранных акций
    }

    fun updFavs(data: ArrayList<StockPojo>){ // Функция для обновления списка избранных акций
        adapter.updateList(data) // Обновляем список в адаптере
        checkFavsUi() // Проверяем пользовательский интерфейс избранных акций
    }

    fun checkFavsUi(){ // Функция для проверки состояния пользовательского интерфейса избранных акций
        val emptyData = if(adapter.itemCount>0) false else true // Проверяем, есть ли избранные акции

        Visible.show(view.favs, !emptyData) // Показываем или скрываем список избранных акций
        Visible.show(view.hintNoFavs, emptyData) // Показываем или скрываем подсказку о том, что нет избранных акций
    }
}