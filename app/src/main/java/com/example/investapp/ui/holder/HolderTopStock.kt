package com.example.investapp.ui.holder

import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import com.example.investapp.Intents
import com.example.investapp.databinding.HolderTopStockBinding
import com.example.investapp.pojo.StockPojo
import com.example.investapp.util.ImageLoader
import com.example.investapp.ui.StockViewActivity

// Класс HolderTopStock, который наследует RecyclerView.ViewHolder для отображения информации о топовых акциях
class HolderTopStock(val view: HolderTopStockBinding) : RecyclerView.ViewHolder(view.root) {

    // Функция для установки данных о акции в соответствующие элементы представления
    fun setStock(item: StockPojo) {
        view.ticker.text = item.ticker
        view.name.text = item.name
        view.ex.text = item.exchange

        ImageLoader.loadAssets(view.logo, item.logo)

        ImageLoader.loadFlag(view.flag, item.country)

        // Устанавливаем обработчик клика на корневой элемент представления
        view.root.setOnClickListener {
            // Переход к экрану с подробной информацией о тикере
            Intents.from(view.root.context).toTicker(item.ticker)
        }
    }
}
