package com.example.investapp.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import com.example.investapp.R
import com.example.investapp.adapter.AdapterFav
import com.example.investapp.api.ApiBuilder
import com.example.investapp.api.callback.SearchApiCallback
import com.example.investapp.databinding.ActivitySearchBinding
import com.example.investapp.pojo.StockPojo
import com.example.investapp.ui.view.MultiView
import com.example.investapp.ui.view.SearchView

class SearchActivity: AppCompatActivity(), SearchApiCallback { // Определяем класс SearchActivity, который реализует интерфейс SearchApiCallback

    lateinit var view: SearchView // Объявляем переменную представления

    override fun onCreate(savedInstanceState: Bundle?) { // Переопределяем метод onCreate для инициализации активности
        super.onCreate(savedInstanceState) // Вызываем метод родительского класса
        view = SearchView(this) // Инициализируем переменную представления передавая контекст активности
    }

    fun startSearch(query: String) { // Определяем метод startSearch для выполнения поиска
        Log.i("DEV", "startSearch: " + query)
        ApiBuilder.search(query, this) // Вызываем метод поиска из ApiBuilder, передавая запрос и текущий объект как коллбек
    }

    override fun showData(results: ArrayList<StockPojo>?) { // Переопределяем метод showData для отображения результатов поиска
        if (results != null && results.size > 0) // Проверяем, что результаты не пустые
            view.showResults(results) // Если результаты есть, отображаем их
        else
            view.showHint(R.string.search_no) // В противном случае показываем подсказку о том, что результатов нет
        view.setBtnVisible(true)
    }

    override fun showFail(isApiError: Boolean) { // Переопределяем метод showFail для обработки ошибок
        Log.e("DEV", "showFail!")
        view.showHint(if (isApiError) R.string.error_api else R.string.error_internet) // Показываем соответствующее сообщение об ошибке
        view.setBtnVisible(true)
    }
}