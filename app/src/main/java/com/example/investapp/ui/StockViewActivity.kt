package com.example.investapp.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.investapp.R
import com.example.investapp.adapter.AdapterValue
import com.example.investapp.api.ApiBuilder
import com.example.investapp.pojo.CompanyInfo
import com.example.investapp.data.ValuesFormatter
import com.example.investapp.api.callback.HistoryApiCallback
import com.example.investapp.api.callback.OverviewApiCallback
import com.example.investapp.data.FavsDB
import com.example.investapp.pojo.chart.ChartEntry
import com.example.investapp.ui.view.MainView
import com.example.investapp.ui.view.StockView
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis.AxisDependency
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet


// Определяем класс StockViewActivity, который реализует интерфейсы OverviewApiCallback и HistoryApiCallback
class StockViewActivity: AppCompatActivity(), OverviewApiCallback, HistoryApiCallback {

    companion object {
        const val TAG = "ticker" // Константа для передачи тикера акций
    }

    lateinit var view: StockView // Объявляем переменную для представления акций
    lateinit var companyInfo: CompanyInfo // Объявляем переменную для хранения информации о компании
    val db = FavsDB(this) // Инициализируем базу данных избранных
    var ticker = "" // Переменная для хранения тикера акций
    var menuFavBtn: MenuItem? = null // Переменная для хранения кнопки избранного в меню

    // Переопределяем метод onCreate для инициализации активности
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        view = StockView(this) // Инициализируем представление акций контекстом активности
        ticker = intent.getStringExtra(TAG).toString() // Получаем тикер акции из интента
        view.setTickerTitle(ticker) // Устанавливаем заголовок
        ApiBuilder.getOverview(ticker, this) // Запрашиваем общую информацию о компании
    }

    // Переопределяем метод onCreateOptionsMenu для создания меню
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu) // Получаем меню из ресурсов
        Log.i("DEV", "create menu " + menu?.size())
        if (menu != null && menu.size() > 0) { // Проверяем, что меню не пустое
            menuFavBtn = menu.getItem(0)
            view.setMenuFavBtn() // Устанавливаем кнопку избранного в представлении
        }
        return super.onCreateOptionsMenu(menu) // Возвращаем результат вызова родительского метода
    }

    // Переопределяем метод onOptionsItemSelected для обработки нажатий на элементы меню
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_fave) view.clickFavBtn() // Обрабатываем нажатие на кнопку избранного
        return super.onOptionsItemSelected(item) // Возвращаем результат вызова родительского метода
    }

    // Переопределяем метод showData для отображения данных о компании
    override fun showData(companyInfo: CompanyInfo) {
        Log.d("DEV", "showData Company!")
        this.companyInfo = companyInfo // Сохраняем информацию о компании
        ApiBuilder.getHistory(ticker, this) // Запрашиваем историю цены акций
    }

    // Переопределяем метод showFail для обработки ошибок API
    override fun showFail(isApiError: Boolean) {
        Log.e("DEV", "fail!")
        view.showError(if (isApiError) R.string.error_api else R.string.error_internet) // Показываем сообщение об ошибке
    }

    // Переопределяем метод showChart для отображения графика
    override fun showChart(history: ArrayList<ChartEntry>) {
        if (!history.isEmpty()) { // Проверяем, что история не пустая
            companyInfo.price = history.last().value // Устанавливаем последнюю цену акций
            view.showChart(history) // Отображаем график
        }
        view.showValues() // Отображаем значения
    }

    // Переопределяем метод showChartFail для обработки ошибок при отображении графика
    override fun showChartFail() {
        view.showValues() // Отображаем значения в случае ошибки
    }
}