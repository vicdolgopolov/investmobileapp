package com.example.investapp.ui.holder

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.investapp.R
import com.example.investapp.databinding.HolderValueBinding
import com.example.investapp.pojo.OverviewValue

// Класс HolderValue, который управляет отображением параметров конкретной акции
class HolderValue(val view: HolderValueBinding) : ViewHolder(view.root) {

    // Функция для установки значений в элементы интерфейса
    fun set(value: OverviewValue) {
        view.name.text = value.key
        view.clipboard.text = value.value

        // Устанавливаем обработчик клика для элемента clipboard
        view.clipboard.setOnClickListener {
            // Копируем текст в буфер обмена
            clip(view.clipboard.context, value.value)
            // Меняем иконку на иконку проверки после копирования
            view.clipboard.setIconResource(R.drawable.ic_copy_check)
        }
    }

    // Функция для копирования текста в буфер обмена
    fun clip(ctx: Context, text: String) {
        // Получаем доступ к системному сервису буфера обмена
        val clipboard = ctx.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        // Создаем новый объект ClipData с текстом для копирования
        val clip: ClipData = ClipData.newPlainText("label", text)
        // Устанавливаем созданный ClipData как основной элемент буфера обмена
        clipboard.setPrimaryClip(clip)
    }
}