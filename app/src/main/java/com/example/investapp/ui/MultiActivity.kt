package com.example.investapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.investapp.R
import com.example.investapp.data.FavsDB
import com.example.investapp.data.MultiFormatter
import com.example.investapp.pojo.CompanyInfo
import com.example.investapp.pojo.MultiValue
import com.example.investapp.ui.view.MainView
import com.example.investapp.ui.view.MultiView

class MultiActivity: AppCompatActivity() {

    companion object { // Объект-компаньон для хранения констант
        const val TAG = "company" // Константа для идентификации компании
    }

    lateinit var view: MultiView // Объявляем переменную для представления мультипликаторов
    lateinit var companyInfo: CompanyInfo // Объявляем переменную для информации о компании

    override fun onCreate(savedInstanceState: Bundle?) { // Переопределяем метод onCreate для инициализации активности
        super.onCreate(savedInstanceState)
        companyInfo = CompanyInfo.create(TAG, savedInstanceState, intent)!! // Создаем объект CompanyInfo с использованием переданных данных
        view = MultiView(this) // Инициализируем представление контекстом активности
        view.setData(MultiFormatter.toList(this, companyInfo)) // Устанавливаем данные в представление
    }
}