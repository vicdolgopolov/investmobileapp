package com.example.investapp.ui.holder

import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import com.example.investapp.Intents
import com.example.investapp.databinding.HolderFavStockBinding
import com.example.investapp.pojo.StockPojo
import com.example.investapp.ui.StockViewActivity

// Класс HolderFavStock, который содержит данные акции для отображения в представлении
class HolderFavStock(val view: HolderFavStockBinding) : RecyclerView.ViewHolder(view.root) {

    fun setStock(item: StockPojo) {
        view.name.text = item.name // Устанавливаем имя акции в текстовое поле
        view.ticker.text = item.ticker // Устанавливаем тикер акции в текстовое поле

        // Устанавливаем обработчик клика на корневой элемент
        view.root.setOnClickListener {
            Intents.from(view.root.context).toTicker(item.ticker) // Переход на экран тикера при клике
        }
    }
}