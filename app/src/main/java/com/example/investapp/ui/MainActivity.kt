package com.example.investapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.investapp.data.FavsDB
import com.example.investapp.ui.view.MainView

class MainActivity : AppCompatActivity() {
    lateinit var view: MainView // Объявляем переменную для представления главного экрана
    lateinit var db: FavsDB // Объявляем переменную для базы данных избранных элементов

    override fun onCreate(bundle: Bundle?) { // Переопределяем метод onCreate для инициализации активности
        super.onCreate(bundle)
        db = FavsDB(this) // Инициализируем базу данных и представление, передавая контекст активности
        view = MainView(this)
    }
    override fun onResume() { // Переопределяем метод onResume, который вызывается при возобновлении активности
        super.onResume() // Вызываем метод родительского класса
        if(FavsDB.ISUPDATE){ // Проверяем, требуется ли обновление данных
            FavsDB.ISUPDATE = false // Сбрасываем флаг обновления
            view.updFavs(db.getFavoritesCache()) // Обновляем представление с новыми данными из базы
        }
    }
}