package com.example.investapp.ui.view

import androidx.recyclerview.widget.LinearLayoutManager
import com.example.investapp.adapter.AdapterFav
import com.example.investapp.adapter.AdapterMulti
import com.example.investapp.databinding.ActivityMainBinding
import com.example.investapp.databinding.ActivityMultiBinding
import com.example.investapp.pojo.MultiValue
import com.example.investapp.ui.MultiActivity

class MultiView(val activity: MultiActivity) {

    var view: ActivityMultiBinding = ActivityMultiBinding.inflate(activity.layoutInflater) // Инициализируем связывание представлений

    init {
        activity.setContentView(view.root) // Устанавливаем корневой элемент привязки как содержимое активности
        activity.setSupportActionBar(view.toolbar) // Устанавливаем toolbar как ActionBar для активности
        view.toolbar.setNavigationOnClickListener { activity.finish() } // Устанавливаем обработчик нажатия для кнопки навигации, чтобы закрыть активность
    }

    fun setData(data: ArrayList<MultiValue>){ // Функция для установки данных в адаптер
        view.data.layoutManager = LinearLayoutManager(activity) // Устанавливаем LinearLayoutManager для RecyclerView
        view.data.adapter = AdapterMulti(data) // Устанавливаем адаптер с переданными данными
    }
}