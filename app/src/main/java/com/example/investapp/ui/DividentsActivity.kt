package com.example.investapp.ui

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.investapp.R
import com.example.investapp.databinding.ActivityDividentsBinding
import com.example.investapp.pojo.CompanyInfo
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.google.android.material.slider.Slider

class DividentsActivity : AppCompatActivity() {

    companion object{
        const val TAG = "company"
    }

    lateinit var view: ActivityDividentsBinding // Объявляем переменную для привязки данных
    var ticker = "" // Переменная для хранения символа компании
    var price = 1f // Переменная для хранения цены акции
    var dividents = 0.02f // Переменная для хранения дивидендов на акцию

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState) // Вызываем метод родительского класса
        view = ActivityDividentsBinding.inflate(layoutInflater) // Инициализируем привязку данных
        setContentView(view.root) // Устанавливаем корневой вид для активности
        setSupportActionBar(view.toolbar) // Устанавливаем панель инструментов
        view.toolbar.setNavigationIcon(R.drawable.ic_back) // Устанавливаем иконку навигации
        view.toolbar.setNavigationOnClickListener {
            finish() // Закрываем активность при нажатии на иконку
        }
        val companyInfo = CompanyInfo.create(TAG, savedInstanceState, intent) // Получаем информацию о компании
        ticker = companyInfo?.symbol.toString() // Получаем символ компании
        try{
            if(companyInfo?.price != null) price = companyInfo.price!! // Устанавливаем цену акции
            if(companyInfo?.dividendPerShare != null) dividents = companyInfo.dividendPerShare.toFloat() // Устанавливаем дивиденды на акцию
        } catch (e:Exception){
            // Обработка исключений
        }
        view.toolbar.title = getString(R.string.dividents) +" $ticker" // Устанавливаем заголовок панели инструментов

        view.sumDividents.setText((price/2).toString()) // Устанавливаем начальное значение дивидендов
        view.sliderDiv.value = dividents // Устанавливаем значение ползунка дивидендов

        view.btnAction.setOnClickListener {
            genSum()
            formula() // Вызываем метод для расчета формулы
        }

        val callback = object : Slider.OnChangeListener{
            override fun onValueChange(p0: Slider, p1: Float, p2: Boolean) {
                if(p2){
                    formula() // Вызываем формулу при изменении значения ползунка
                }
            }
        }

        view.sliderPrice.addOnChangeListener(callback) // Добавляем слушатель на изменение ползунка цены
        view.sliderDiv.addOnChangeListener(callback) // Добавляем слушатель на изменение ползунка дивидендов
        setChartUi() // Устанавливаем интерфейс графика
        genSum() //суммируем
        formula() // Вызываем формулу

    }

    fun setChartUi() {
        view.chart.description.isEnabled = false // Отключаем описание графика
        view.chart.axisRight.isEnabled = false // Отключаем правую ось графика
        view.chart.axisLeft.axisMinimum = 0f // Устанавливаем минимальное значение для левой оси
        view.chart.xAxis.position = XAxis.XAxisPosition.BOTTOM_INSIDE // Устанавливаем позицию оси X
        view.chart.xAxis.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                val i = value.toInt()
                return i.toString()
            }
        }
    }

    fun genSum() {
        val count = view.count.text.toString().toFloat() // Получаем количество акций
        val sum = count * price // Вычисляем общую сумму
        view.sumShares.text = "$ $sum" // Устанавливаем текст
    }

    fun formula() {
        val result = view.sumDividents.text.toString().toFloat() // Получаем значение дивидендов
        val chartSum = ArrayList<Entry>() // Создаем список для суммы
        val chartDiv = ArrayList<Entry>() // Создаем список для дивидендов

        val count = view.count.text.toString().toFloat() // Получаем количество акций
        var sum = count * price // Вычисляем общую сумму

        var divAg = (sum / 100) * view.sliderDiv.value // Вычисляем дивиденды

        chartSum.add(Entry(2024f, sum)) // Добавляем данные суммы в график
        chartDiv.add(Entry(2024f, divAg)) // Добавляем данные дивидендов в график
        var year = 2025 // Начинаем с 2025 года

        while (divAg < result) { // Пока дивиденды меньше результата
            val yearAg = (sum / 100) * view.sliderPrice.value // Вычисляем годовые дивиденды
            divAg = (sum / 100) * view.sliderDiv.value * 4 // Обновляем дивиденды

            sum += yearAg // Обновляем общую сумму

            Log.i("DEV","div $year = $divAg")
            chartDiv.add(Entry(year.toFloat(), divAg)) // Добавляем данные дивидендов в график
            year += 1 // Переходим к следующему году
        }

        val linesSum = LineDataSet(chartSum, "Сумма") // Создаем набор данных для суммы
        linesSum.setColor(Color.GREEN) // Устанавливаем цвет линии суммы
        linesSum.setDrawCircleHole(false) // Отключаем рисование отверстий в круге
        linesSum.lineWidth = 1.2f // Устанавливаем ширину линии суммы
        val linesDiv = LineDataSet(chartDiv, "Дивиденты") // Создаем набор данных для дивидендов
        linesDiv.setColor(Color.YELLOW) // Устанавливаем цвет линии дивидендов
        linesDiv.setDrawCircleHole(false) // Отключаем рисование отверстий в круге
        linesDiv.setDrawCircles(false) // Отключаем рисование кругов
        linesDiv.setDrawValues(false) // Отключаем отображение значений
        linesDiv.lineWidth = 2.2f // Устанавливаем ширину линии дивидендов
        linesDiv.valueTextSize = 10f // Устанавливаем размер текста значений

        val dataSets = ArrayList<ILineDataSet>() // Создаем список наборов данных
        dataSets.add(linesSum) // Добавляем набор данных суммы (закомментировано)
        dataSets.add(linesDiv) // Добавляем набор данных дивидендов

        view.chart.setData(LineData(dataSets)) // Устанавливаем данные для графика
        view.chart.invalidate() // Обновляем график
    }
}