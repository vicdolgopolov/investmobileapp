package com.example.investapp.ui.view

import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.annotation.StringRes
import com.example.investapp.R
import com.example.investapp.adapter.AdapterFav
import com.example.investapp.databinding.ActivityMultiBinding
import com.example.investapp.databinding.ActivitySearchBinding
import com.example.investapp.pojo.StockPojo
import com.example.investapp.ui.SearchActivity

class SearchView(val activity: SearchActivity) {
        // Инициализация привязки представления для активности поиска
        var view: ActivitySearchBinding = ActivitySearchBinding.inflate(activity.layoutInflater)

        init {
            // Установка корневого представления активности
            activity.setContentView(view.root)

            // Установка слушателя для действия ввода текста
            view.search.setOnEditorActionListener { textView, actionId, keyEvent ->
                // Проверка, было ли действие поиска
                if (actionId == EditorInfo.IME_ACTION_SEARCH) return@setOnEditorActionListener preSearch()
                return@setOnEditorActionListener false
            }

            // Установка слушателя для кнопки поиска
            view.searchBtnAction.setOnClickListener { preSearch() }
            // Установка фокуса на поле ввода поиска
            view.search.requestFocus()
        }

        // Метод для предварительного поиска
        private fun preSearch(): Boolean {
            // Получение текста из поля ввода
            val query = view.search.text.toString()
            // Проверка, не пустой ли запрос
            if (query.isNotEmpty()) {
                // Скрытие кнопки и других элементов интерфейса
                setBtnVisible(false)
                view.stocks.visibility = View.GONE
                view.searchHint.visibility = View.INVISIBLE
                // Запуск поиска с введенным запросом
                activity.startSearch(query)
                return true
            }
            return false
        }

        // Метод для установки видимости кнопки
        fun setBtnVisible(visible: Boolean) {
            view.searchBtnAction.visibility = if (visible) View.VISIBLE else View.INVISIBLE
            view.progress.visibility = if (visible) View.INVISIBLE else View.VISIBLE
            view.anim.visibility = if (visible) View.INVISIBLE else View.VISIBLE
        }

        // Метод для отображения подсказки
        fun showHint(@StringRes msg: Int) {
            view.searchHint.setText(msg)
            view.searchHint.visibility = View.VISIBLE
        }

        // Метод для отображения результатов поиска
        fun showResults(results: ArrayList<StockPojo>) {
            // Установка адаптера для отображения результатов
            view.stocks.adapter = AdapterFav(results)
            // Отображение списка результатов
            view.stocks.visibility = View.VISIBLE
        }
}