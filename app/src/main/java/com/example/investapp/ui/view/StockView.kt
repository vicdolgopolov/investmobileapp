package com.example.investapp.ui.view

import android.content.Intent
import android.graphics.Color
import android.telephony.CarrierConfigManager.Gps
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.annotation.StringRes
import com.example.investapp.Intents
import com.example.investapp.R
import com.example.investapp.adapter.AdapterValue
import com.example.investapp.data.ValuesFormatter
import com.example.investapp.databinding.ActivityStockBinding
import com.example.investapp.pojo.chart.ChartEntry
import com.example.investapp.ui.StockViewActivity
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet

class StockView(val activity: StockViewActivity) {

    var view: ActivityStockBinding = ActivityStockBinding.inflate(activity.layoutInflater) // Инициализируем связывание представлений

    init {
        activity.setContentView(view.root) // Устанавливаем корневое представление активности
        activity.setSupportActionBar(view.toolbar) // Устанавливаем панель инструментов
        view.toolbar.setNavigationOnClickListener { activity.finish() } // Обработчик нажатия на кнопку навигации
        view.btnMulti.setOnClickListener { Intents.from(activity).calcMulti(activity.companyInfo) } // Обработчик нажатия на кнопку "Мульти"
        view.btnDividents.setOnClickListener { Intents.from(activity).calcDividents(activity.companyInfo) } // Обработчик нажатия на кнопку "Дивиденды"

        initChartUi() // Инициализация пользовательского интерфейса графика
    }

    fun setTickerTitle(ticker: String) { // Функция для установки заголовка тикера
        view.toolbar.title = ticker
    }

    fun setMenuVisible(b: Boolean) { // Функция для установки видимости меню
        Log.i("DEV", "menuVISIBLE")
        activity.menuFavBtn?.setVisible(b) // Устанавливаем видимость кнопки "Избранное"
    }

    fun setMenuFavBtn() { // Функция для настройки кнопки "Избранное"
        activity.menuFavBtn?.setIcon(if (activity.db.isFav(activity.ticker)) R.drawable.ic_star_filled else R.drawable.ic_star) // Устанавливаем иконку в зависимости от состояния "Избранное"
        activity.menuFavBtn?.setTitle(if (activity.db.isFav(activity.ticker)) R.string.fav_rm else R.string.fav_add) // Устанавливаем заголовок в зависимости от состояния "Избранное"
        setMenuVisible(false)
    }

    fun clickFavBtn() { // Функция для обработки нажатия на кнопку "Избранное"
        if (activity.menuFavBtn?.title.toString().equals(activity.getString(R.string.fav_add))) { // Проверяем, добавлено ли в избранное
            activity.db.add(activity.ticker, activity.companyInfo.name) // Добавляем тикер в избранное
            activity.menuFavBtn?.setIcon(R.drawable.ic_star_filled) // Устанавливаем иконку "Избранное"
            activity.menuFavBtn?.setTitle(R.string.fav_rm) // Устанавливаем заголовок "Удалить из избранного"
        } else {
            activity.db.remove(activity.ticker) // Удаляем тикер из избранного
            activity.menuFavBtn?.setIcon(R.drawable.ic_star)
            activity.menuFavBtn?.setTitle(R.string.fav_add)
        }
    }

    fun showError(@StringRes msg: Int) { // Функция для отображения ошибки
        view.error.setText(msg)
        view.progress.visibility = View.GONE
        view.error.visibility = View.VISIBLE
    }

    fun showValues() { // Функция для отображения значений
        setMenuVisible(true) // Показываем меню
        view.progress.visibility = View.GONE // Скрываем индикатор загрузки
        view.values.adapter = AdapterValue(ValuesFormatter.toList(activity.companyInfo)) // Устанавливаем адаптер для значений
        view.values.visibility = View.VISIBLE // Показываем значения
        view.btnMulti.visibility = View.VISIBLE // Показываем кнопку "Мультипликаторы"
        view.btnDividents.visibility = View.VISIBLE // Показываем кнопку "Дивиденды"
    }

    fun initChartUi() { // Функция для инициализации пользовательского интерфейса графика
        view.chart.description.isEnabled = false
        view.chart.legend.isEnabled = false
        view.chart.isDoubleTapToZoomEnabled = false

        view.chart.axisRight.isEnabled = false // Отключаем правую ось графика
        view.chart.xAxis.position = XAxis.XAxisPosition.BOTTOM // Устанавливаем позицию оси X внизу
    }

    fun showChart(history: ArrayList<ChartEntry>) { // Функция для отображения графика
        val values = ArrayList<Entry>() // Создаем список значений для графика
        for (i in 0..history.size - 1) values.add(Entry(i.toFloat(), history[i].value)) // Заполняем список значениями из истории

        val lineDataSet = LineDataSet(values, "history") // Создаем набор данных

        lineDataSet.setDrawCircles(false) // Отключаем отображение кругов на графике
        lineDataSet.setColors(Color.parseColor("#14274E"))
        lineDataSet.axisDependency = YAxis.AxisDependency.LEFT // Устанавливаем зависимость от левой оси
        lineDataSet.lineWidth = 2f

        view.chart.axisLeft.labelCount = 5 // Устанавливаем количество меток на левой оси
        view.chart.xAxis.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String { // Переопределяем метод форматирования
                return history[value.toInt()].time // Возвращаем время из истории
            }
        }

        val dataSets = ArrayList<ILineDataSet>() // Создаем список наборов данных
        dataSets.add(lineDataSet) // Добавляем набор данных в список

        view.chart.setData(LineData(dataSets)) // Устанавливаем данные для графика
        view.chart.invalidate() // Обновляем график

        view.chart.visibility = View.VISIBLE // Показываем график
    }
}