package com.example.investapp

import android.content.Context
import android.content.Intent
import com.example.investapp.pojo.CompanyInfo
import com.example.investapp.ui.DividentsActivity
import com.example.investapp.ui.MultiActivity
import com.example.investapp.ui.StockViewActivity
import com.example.investapp.ui.SearchActivity

class Intents(val ctx: Context) { // Определяем класс Intents, принимающий контекст приложения

    companion object { // Определяем компаньон-объект для создания экземпляра класса
        fun from(ctx: Context): Intents {
            return Intents(ctx)
        }
    }

    fun toSearch() { // Функция для запуска SearchActivity
        ctx.startActivity(Intent(ctx, SearchActivity::class.java)) //
    }

    fun toTicker(ticker: String) { // Функция для запуска StockViewActivity
        ctx.startActivity(Intent(ctx, StockViewActivity::class.java)
            .putExtra(StockViewActivity.TAG, ticker)) // Передаем тикер как дополнительную информацию
    }

    fun calcMulti(company: CompanyInfo) { // Функция для запуска MultiActivity
        ctx.startActivity(Intent(ctx, MultiActivity::class.java)
            .putExtra(MultiActivity.TAG, company.toJson())) // Передаем информацию о компании в формате JSON
    }

    fun calcDividents(company: CompanyInfo) { // Функция для запуска DividentsActivity
        ctx.startActivity(Intent(ctx, DividentsActivity::class.java)
            .putExtra(DividentsActivity.TAG, company.toJson())) // Передаем информацию о компании в формате JSON
    }
}