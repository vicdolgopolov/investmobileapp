package com.example.investapp.data

import android.content.Context
import com.example.investapp.R
import com.example.investapp.pojo.CompanyInfo
import com.example.investapp.pojo.MultiValue
import java.math.BigDecimal
import java.math.RoundingMode

// Класс MultiFormatter отвечает за форматирование данных о мультипликаторах
class MultiFormatter(val companyInfo: CompanyInfo) {

    companion object {
        // Функция toList создает список MultiValue на основе информации о компании
        fun toList(ctx: Context, companyInfo: CompanyInfo): ArrayList<MultiValue> {

            // Создаем экземпляр MultiFormatter для форматирования данных
            val format = MultiFormatter(companyInfo)
            val data = ArrayList<MultiValue>()

            data.add(
                MultiValue(ctx.getString(R.string.multi_title_1), ctx.getString(R.string.multi_text_1), ctx.getString(
                    R.string.multi_key_1) + " = ${"%.2f".format(format.getPe())}")
            )

            data.add(
                MultiValue(ctx.getString(R.string.multi_title_2), ctx.getString(R.string.multi_text_2), ctx.getString(
                    R.string.multi_key_2) + " = ${"%.2f".format(format.getPs())}")
            )

            data.add(
                MultiValue(ctx.getString(R.string.multi_title_3), ctx.getString(R.string.multi_text_3), ctx.getString(
                    R.string.multi_key_3) + " = ${companyInfo.ebitda}")
            )

            data.add(
                MultiValue(ctx.getString(R.string.multi_title_4), ctx.getString(R.string.multi_text_4), ctx.getString(
                    R.string.multi_key_4) + " = ${companyInfo.evToEBITDA}")
            )
            data.add(
                MultiValue(ctx.getString(R.string.multi_title_5), ctx.getString(R.string.multi_text_5), ctx.getString(
                    R.string.multi_key_5) + " = ${companyInfo.eps}")
            )

            // Возвращаем заполненный список данных
            return data
        }
    }

    // Функция getPe вычисляет коэффициент P/E
    fun getPe(): Float? {
        // Получаем значение EPS, если оно не null, иначе устанавливаем 0
        val eps = if (companyInfo.eps != null) companyInfo.eps.toFloat() else 0f
        // Возвращаем отношение цены к EPS
        return companyInfo.price?.div(eps)
    }

    // Функция getPs вычисляет коэффициент P/S
    fun getPs(): Float? {
        // Получаем значение выручки на акцию, если оно не null, иначе устанавливаем 0
        val eps = if (companyInfo.revenuePerShareTTM != null) companyInfo.revenuePerShareTTM.toFloat() else 0f
        // Возвращаем отношение цены к выручке на акцию
        return companyInfo.price?.div(eps)
    }
}