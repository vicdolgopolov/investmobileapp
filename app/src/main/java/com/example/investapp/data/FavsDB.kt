package com.example.investapp.data

import android.content.Context
import android.content.SharedPreferences
import com.example.investapp.pojo.StockPojo
import com.example.investapp.pojo.StockPojoData
import com.google.gson.Gson

class FavsDB(val ctx: Context) {

    companion object {
        // Имя базы данных для кэширования избранных акций
        val NAME_DB = "FAVORIES_DB_CACHE"
        // Имя списка для хранения данных
        val NAME_LIST = "data"

        // Флаг обновления данных
        var ISUPDATE = true
    }

    // Метод для добавления акции в избранное
    fun add(ticker: String, name: String?) {
        ISUPDATE = true // Устанавливаем флаг обновления
        val data = getFavoritesCache() // Получаем текущий кэш избранных акций
        // Добавляем новую акцию в кэш
        data.add(StockPojo(name = name.toString(), ticker = ticker))

        // Сохраняем состояние акции как избранное
        getPrefs().edit().putBoolean(ticker, true).apply()
        // Сохраняем обновленный список акций в SharedPreferences
        getPrefs().edit().putString(NAME_LIST, Gson().toJson(StockPojoData(data))).apply()
    }

    // Метод для удаления акции из избранного
    fun remove(ticker: String) {
        ISUPDATE = true // Устанавливаем флаг обновления
        val data = ArrayList<StockPojo>() // Создаем новый список для хранения оставшихся акций
        val oldData = getFavoritesCache() // Получаем текущий кэш избранных акций

        // Проходим по старым данным и добавляем только те акции, которые не совпадают с удаляемой
        for (item in oldData) if (!item.ticker.equals(ticker)) data.add(item)

        // Сохраняем состояние акции как не избранное
        getPrefs().edit().putBoolean(ticker, false).apply()
        // Сохраняем обновленный список акций в SharedPreferences
        getPrefs().edit().putString(NAME_LIST, Gson().toJson(StockPojoData(data))).apply()
    }

    // Метод для проверки в избранном ли акция
    fun isFav(ticker: String): Boolean {
        // Возвращаем состояние акции из SharedPreferences
        return getPrefs().getBoolean(ticker, false)
    }

    // Метод для получения кэша избранных акций
    fun getFavoritesCache(): ArrayList<StockPojo> {
        val json = getPrefs().getString(NAME_LIST, null) // Получаем строку JSON из SharedPreferences
        val data = Gson().fromJson(json, StockPojoData::class.java) // Десериализуем JSON в объект StockPojoData
        // Если данные не пустые, возвращаем их
        if (data?.data != null) return data.data
        // В противном случае возвращаем пустой список
        return ArrayList<StockPojo>()
    }

    // Приватный метод для получения SharedPreferences для работы с кэшем
    private fun getPrefs(): SharedPreferences {
        return ctx.getSharedPreferences(NAME_DB, 0)
    }
}