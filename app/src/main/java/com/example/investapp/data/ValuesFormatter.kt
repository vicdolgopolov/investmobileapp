package com.example.investapp.data

import com.example.investapp.pojo.CompanyInfo
import com.example.investapp.pojo.OverviewValue

// Класс ValuesFormatter отвечает за форматирование данных о компании
class ValuesFormatter {
    companion object{
        // Определяем компаньон-объект для доступа к методам без создания экземпляра класса
        fun toList(body: CompanyInfo): ArrayList<OverviewValue> {
            val values = ArrayList<OverviewValue>()
            if(body.price!=null) values.add(OverviewValue("Price", body.price.toString()))
            if(body.symbol!=null) values.add(OverviewValue("Symbol", body.symbol))
            if(body.assetType!=null) values.add(OverviewValue("Asset Type", body.assetType))
            if(body.name!=null) values.add(OverviewValue("Name", body.name))
            if(body.cik!=null) values.add(OverviewValue("CIK", body.cik))
            if(body.exchange!=null) values.add(OverviewValue("Exchange", body.exchange))
            if(body.currency!=null) values.add(OverviewValue("Currency", body.currency))
            if(body.country!=null) values.add(OverviewValue("Country", body.country))
            if(body.sector!=null) values.add(OverviewValue("Sector", body.sector))
            if(body.industry!=null) values.add(OverviewValue("Industry", body.industry))
            if(body.address!=null) values.add(OverviewValue("Address", body.address))
            if(body.fiscalYearEnd!=null) values.add(OverviewValue("Fiscal Year End", body.fiscalYearEnd))
            if(body.latestQuarter!=null) values.add(OverviewValue("Latest Quarter", body.latestQuarter))
            if(body.marketCapitalization!=null) values.add(OverviewValue("Market Capitalization", body.marketCapitalization))
            //if(body.ebitda!=null) values.add(OverviewValue("EBITDA", body.ebitda))
            //if(body.peRatio!=null) values.add(OverviewValue("PERatio", body.peRatio))
            if(body.pegRatio!=null) values.add(OverviewValue("PEGRatio", body.pegRatio))
            if(body.bookValue!=null) values.add(OverviewValue("BookValue", body.bookValue))
            if(body.dividendPerShare!=null) values.add(OverviewValue("Dividend Per Share", body.dividendPerShare))
            if(body.dividendYield!=null) values.add(OverviewValue("Dividend Yield", body.dividendYield))
            if(body.eps!=null) values.add(OverviewValue("EPS", body.eps))
            if(body.revenuePerShareTTM!=null) values.add(OverviewValue("Revenue Per Share TTM", body.revenuePerShareTTM))
            if(body.profitMargin!=null) values.add(OverviewValue("Profit Margin", body.profitMargin))
            if(body.operatingMarginTTM!=null) values.add(OverviewValue("Operating Margin TTM", body.operatingMarginTTM))
            if(body.returnOnAssetsTTM!=null) values.add(OverviewValue("Return On Assets TTM", body.returnOnAssetsTTM))
            if(body.returnOnEquityTTM!=null) values.add(OverviewValue("Return On Equity TTM", body.returnOnEquityTTM))
            if(body.revenueTTM!=null) values.add(OverviewValue("Revenue TTM", body.revenueTTM))
            if(body.grossProfitTTM!=null) values.add(OverviewValue("Gross Profit TTM", body.grossProfitTTM))
            if(body.dilutedEPSTTM!=null) values.add(OverviewValue("Diluted EPS TTM", body.dilutedEPSTTM))
            if(body.quarterlyEarningsGrowthYOY!=null) values.add(OverviewValue("Quarterly Earnings Growth YOY", body.quarterlyEarningsGrowthYOY))
            if(body.analystTargetPrice!=null) values.add(OverviewValue("Analyst Target Price", body.analystTargetPrice))
            if(body.analystRatingStrongBuy!=null) values.add(OverviewValue("Analyst Rating Strong Buy", body.analystRatingStrongBuy))
            if(body.analystRatingBuy!=null) values.add(OverviewValue("Analyst Rating Buy", body.analystRatingBuy))
            if(body.analystRatingHold!=null) values.add(OverviewValue("Analyst Rating Hold", body.analystRatingHold))
            if(body.analystRatingSell!=null) values.add(OverviewValue("Analyst Rating Sell", body.analystRatingSell))
            if(body.analystRatingStrongSell!=null) values.add(OverviewValue("Analyst Rating Strong Sell", body.analystRatingStrongSell))
            if(body.trailingPE!=null) values.add(OverviewValue("Trailing PE", body.trailingPE))
            if(body.forwardPE!=null) values.add(OverviewValue("Forward PE", body.forwardPE))
            if(body.priceToSalesRatioTTM!=null) values.add(OverviewValue("Price To Sales Ratio TTM", body.priceToSalesRatioTTM))
            if(body.priceToBookRatio!=null) values.add(OverviewValue("Price To Book Ratio", body.priceToBookRatio))
            //if(body.evToRevenue!=null) values.add(OverviewValue("EVTo Revenue", body.evToRevenue))
            //if(body.evToEBITDA!=null) values.add(OverviewValue("EVTo EBITDA", body.evToEBITDA))
            if(body.beta!=null) values.add(OverviewValue("Beta", body.beta))
            if(body.sharesOutstanding!=null) values.add(OverviewValue("Shares Outstanding", body.sharesOutstanding))
            if(body.dividendDate!=null) values.add(OverviewValue("Dividend Date", body.dividendDate))
            if(body.exDividendDate!=null) values.add(OverviewValue("ExDividend Date", body.exDividendDate))

            return values
        }
    }
}