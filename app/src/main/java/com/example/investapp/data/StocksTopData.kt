package com.example.investapp.data

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.example.investapp.pojo.StockPojo
import com.example.investapp.pojo.StockPojoData

// Класс для работы с данными топовых акций
class StocksTopData {
    companion object {
        // Функция для получения списка
        fun getTop(ctx: Context): ArrayList<StockPojo> {
            // Получаем JSON-строку из файла
            val line = getJson(ctx)
            Log.i("DEV", line)
            // Создаем экземпляр Gson для парсинга JSON
            val gson = Gson()
            // Парсим JSON-строку в объект StockPojoData
            val data = gson.fromJson(line, StockPojoData::class.java)
            // Возвращаем список акций из объекта StockPojoData
            return data.data!!
        }

        // Приватная функция для чтения JSON из файла
        private fun getJson(ctx: Context): String {
            var json = ""
            try {
                // Открываем поток для чтения файла top100.json из assets
                val stream = ctx.assets.open("top100.json")
                // Получаем размер файла
                val size = stream.available()
                // Создаем буфер для чтения данных
                val buffer = ByteArray(size)
                // Читаем данные из потока в буфер
                stream.read(buffer)
                // Закрываем поток после чтения
                stream.close()
                // Преобразуем буфер в строку
                json = String(buffer)

            } catch (e: Exception) {
                Log.e("DEV", e.message.toString())
            }

            // Возвращаем считанную JSON-строку
            return json
        }
    }
}