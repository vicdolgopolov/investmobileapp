package com.example.investapp.util

import android.net.Uri
import android.util.Log
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.investapp.Config
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou

class ImageLoader {
    companion object { // Определяем компаньон-объект для статических методов
        fun set(iv: ImageView, uri: String) { // Метод для загрузки изображения по URI
            Glide
                .with(iv) // Указываем контекст загрузки
                .load(uri) // Загружаем изображение по указанному URI
                .diskCacheStrategy(DiskCacheStrategy.ALL) // Устанавливаем стратегию кэширования
                .into(iv) // Вставляем загруженное изображение в ImageView
        }

        fun loadFlag(iv: ImageView, flag: String) { // Метод для загрузки флага
            val uri = Uri.parse(Config.flagUrl(flag.lowercase())) // Преобразуем URL флага в URI

            GlideToVectorYou.init() // Инициализируем GlideToVectorYou
                .with(iv.context) // Указываем контекст для загрузки
                .load(uri, iv) // Загружаем векторное изображение по URI в ImageView
        }

        fun loadAssets(iv: ImageView, file: String) { // Метод для загрузки изображений из папки assets
            set(iv, "file:///android_asset/$file") // Вызываем метод set с URI для файла в assets
        }
    }
}