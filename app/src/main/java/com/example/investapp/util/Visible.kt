package com.example.investapp.util

import android.view.View

class Visible {
    companion object{
        fun show(v: View, v2:Boolean){
            v.visibility = if(v2) View.VISIBLE else View.GONE
        }
    }
}