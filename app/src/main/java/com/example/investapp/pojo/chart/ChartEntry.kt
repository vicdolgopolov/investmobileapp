package com.example.investapp.pojo.chart

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


// Класс служит для десериализации и представления данных точки на графике
data class ChartEntry(
    @Expose @SerializedName("time") val timeLong:String,
    @Expose @SerializedName("time_short") val time:String,
    @Expose val high: Float,
    @Expose val low: Float,
    @Expose val open: Float,
    @Expose @SerializedName("close") val value: Float
)
