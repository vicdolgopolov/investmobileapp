package com.example.investapp.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

// Класс служит для сериализации и десериализации массива акций
data class StockPojoData(
    @Expose @SerializedName("bestMatches") val data:ArrayList<StockPojo>?
)
