package com.example.investapp.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

// Класс служит для десериализации и представления общих данных об акции, получаемых из API
data class StockPojo(
    @Expose @SerializedName("1. symbol") var ticker: String,
    @Expose @SerializedName("2. name")  var name: String = "",
    @Expose var logo: String = "",
    @Expose @SerializedName("ex") var exchange: String = "",
    @Expose var country: String = ""
)
