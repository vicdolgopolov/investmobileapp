package com.example.investapp.pojo

//Класс служит для хранения данных о финансовом показателе конкретной акции
data class OverviewValue(
    val key:String,
    val value:String
)
