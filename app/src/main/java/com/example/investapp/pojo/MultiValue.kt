package com.example.investapp.pojo

//Класс служит для хранения данных о мультипликаторе конкретной акции

data class MultiValue(
    val name: String,
    val text:String,
    val value:String
)
