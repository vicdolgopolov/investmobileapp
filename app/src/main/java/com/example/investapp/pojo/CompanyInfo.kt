package com.example.investapp.pojo

import android.content.Intent
import android.os.Bundle
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

// Класс служит для сериализации, десериализации, представления подробных данных об акции, получаемых из API
data class CompanyInfo(
    var price:Float?,
    @SerializedName("Symbol") val symbol: String?,
    @SerializedName("AssetType") val assetType: String?,
    @SerializedName("Name") val name: String?,
    @SerializedName("Description") val description: String?,
    @SerializedName("CIK") val cik: String?,
    @SerializedName("Exchange") val exchange: String?,
    @SerializedName("Currency") val currency: String?,
    @SerializedName("Country") val country: String?,
    @SerializedName("Sector") val sector: String?,
    @SerializedName("Industry") val industry: String?,
    @SerializedName("Address") val address: String?,
    @SerializedName("FiscalYearEnd") val fiscalYearEnd: String?,
    @SerializedName("LatestQuarter") val latestQuarter: String?,
    @SerializedName("MarketCapitalization") val marketCapitalization: String?,
    @SerializedName("EBITDA") val ebitda: String?,
    @SerializedName("PERatio") val peRatio: String?,
    @SerializedName("PEGRatio") val pegRatio: String?,
    @SerializedName("BookValue") val bookValue: String?,
    @SerializedName("DividendPerShare") val dividendPerShare: String?,
    @SerializedName("DividendYield") val dividendYield: String?,
    @SerializedName("EPS") val eps: String?,
    @SerializedName("RevenuePerShareTTM") val revenuePerShareTTM: String?,
    @SerializedName("ProfitMargin") val profitMargin: String?,
    @SerializedName("OperatingMarginTTM") val operatingMarginTTM: String?,
    @SerializedName("ReturnOnAssetsTTM") val returnOnAssetsTTM: String?,
    @SerializedName("ReturnOnEquityTTM") val returnOnEquityTTM: String?,
    @SerializedName("RevenueTTM") val revenueTTM: String?,
    @SerializedName("GrossProfitTTM") val grossProfitTTM: String?,
    @SerializedName("DilutedEPSTTM") val dilutedEPSTTM: String?,
    @SerializedName("QuarterlyEarningsGrowthYOY") val quarterlyEarningsGrowthYOY: String?,
    @SerializedName("QuarterlyRevenueGrowthYOY") val quarterlyRevenueGrowthYOY: String?,
    @SerializedName("AnalystTargetPrice") val analystTargetPrice: String?,
    @SerializedName("AnalystRatingStrongBuy") val analystRatingStrongBuy: String?,
    @SerializedName("AnalystRatingBuy") val analystRatingBuy: String?,
    @SerializedName("AnalystRatingHold") val analystRatingHold: String?,
    @SerializedName("AnalystRatingSell") val analystRatingSell: String?,
    @SerializedName("AnalystRatingStrongSell") val analystRatingStrongSell: String?,
    @SerializedName("TrailingPE") val trailingPE: String?,
    @SerializedName("ForwardPE") val forwardPE: String?,
    @SerializedName("PriceToSalesRatioTTM") val priceToSalesRatioTTM: String?,
    @SerializedName("PriceToBookRatio") val priceToBookRatio: String?,
    @SerializedName("EVToRevenue") val evToRevenue: String?,
    @SerializedName("EVToEBITDA") val evToEBITDA: String?,
    @SerializedName("Beta") val beta: String?,
    @SerializedName("52WeekHigh") val week52High: String?,
    @SerializedName("52WeekLow") val week52Low: String?,
    @SerializedName("50DayMovingAverage") val movingAverage50Day: String?,
    @SerializedName("200DayMovingAverage") val movingAverage200Day: String?,
    @SerializedName("SharesOutstanding") val sharesOutstanding: String?,
    @SerializedName("DividendDate") val dividendDate: String?,
    @SerializedName("ExDividendDate") val exDividendDate: String?
){
    fun toJson():String{
        return Gson().toJson(this)
    }

    companion object {
        // Метод для создания объекта CompanyInfo из Bundle или Intent
        fun create(tag: String, bundle: Bundle?, intent: Intent): CompanyInfo? {
            // Проверяем наличие ключа в Bundle и десериализуем объект
            if (bundle != null && bundle.containsKey(tag)) return Gson().fromJson(bundle.getString(tag), CompanyInfo::class.java)

            // Если ключа нет в Bundle, десериализуем объект из Intent
            return Gson().fromJson(intent.getStringExtra(tag), CompanyInfo::class.java)
        }
    }
}
