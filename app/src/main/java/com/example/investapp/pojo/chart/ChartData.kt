package com.example.investapp.pojo.chart

import android.service.autofill.FillEventHistory
import com.google.gson.annotations.Expose

//Класс служит для представления данных о графике акции, получаемых из API

data class ChartData(
    @Expose val history: ArrayList<ChartEntry>?
)
