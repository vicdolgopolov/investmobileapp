package com.example.investapp

object Config {

    fun flagUrl(flag:String):String{
        return "https://hatscripts.github.io/circle-flags/flags/$flag.svg"
    }

    const val BASE_URL = "https://stockshistoryalpha.webtm.ru/api/"

}