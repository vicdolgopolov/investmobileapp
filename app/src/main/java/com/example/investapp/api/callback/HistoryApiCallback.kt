package com.example.investapp.api.callback

import com.example.investapp.pojo.chart.ChartEntry

interface HistoryApiCallback {
    fun showChart(history: ArrayList<ChartEntry>)
    fun showChartFail()
}