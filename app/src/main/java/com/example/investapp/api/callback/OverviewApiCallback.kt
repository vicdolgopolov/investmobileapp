package com.example.investapp.api.callback

import com.example.investapp.pojo.CompanyInfo

interface OverviewApiCallback {
    fun showData(companyInfo: CompanyInfo)
    fun showFail(isApiError: Boolean)
}