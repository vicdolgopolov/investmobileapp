package com.example.investapp.api

import android.util.Log
import com.example.investapp.api.callback.HistoryApiCallback
import com.example.investapp.api.callback.OverviewApiCallback
import com.example.investapp.api.callback.SearchApiCallback
import com.example.investapp.Config
import com.example.investapp.pojo.CompanyInfo
import com.example.investapp.pojo.StockPojoData
import com.example.investapp.pojo.chart.ChartData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiBuilder() {

    companion object {
        // Функция для поиска акций по запросу
        fun search(query: String, callback: SearchApiCallback) {
            // Создание экземпляра Retrofit с базовым URL
            Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(StockApi::class.java)
                .search(query) // Выполнение запроса на поиск
                .enqueue(object : Callback<StockPojoData> {
                    override fun onResponse(
                        call: Call<StockPojoData>,
                        response: Response<StockPojoData>
                    ) {
                        val body = response.body() // Получение тела ответа
                        // Проверка успешности ответа и наличия данных
                        if (response.isSuccessful && body != null && body.data != null)
                            callback.showData(body.data) // Отображение данных
                        else
                            callback.showFail(true) // Обработка ошибки
                    }

                    override fun onFailure(call: Call<StockPojoData>, t: Throwable) {
                        Log.e("DEV", t.message.toString())
                        callback.showFail(false) // Обработка ошибки
                    }
                })
        }

        // Функция для получения информации о компании по тикеру
        fun getOverview(ticker: String, callback: OverviewApiCallback) {
            // Создание экземпляра Retrofit
            Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(StockApi::class.java)
                .getCompanyInfo(ticker) // Выполнение запроса на получение информации о компании
                .enqueue(object : Callback<CompanyInfo> {
                    override fun onResponse(
                        call: Call<CompanyInfo>,
                        response: Response<CompanyInfo>
                    ) {
                        val body = response.body() // Получение тела ответа
                        // Проверка успешности ответа и наличия символа компании
                        if (response.isSuccessful && body != null && body.symbol != null)
                            callback.showData(body) // Отображение данных
                        else
                            callback.showFail(true) // Обработка ошибки
                    }

                    override fun onFailure(call: Call<CompanyInfo>, t: Throwable) {
                        callback.showFail(false) // Обработка ошибки
                    }
                })
        }

        // Функция для получения истории акций по тикеру
        fun getHistory(ticker: String, callback: HistoryApiCallback) {
            // Создание экземпляра Retrofit
            Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(StockApi::class.java)
                .getHistory(ticker) // Выполнение запроса на получение истории
                .enqueue(object : Callback<ChartData> {
                    override fun onResponse(call: Call<ChartData>, response: Response<ChartData>) {
                        val body = response.body() // Получение тела ответа
                        // Проверка успешности ответа и наличия истории
                        if (response.isSuccessful && body != null && body.history != null)
                            callback.showChart(body.history) // Отображение графика
                        else
                            callback.showChartFail() // Обработка ошибки
                    }

                    override fun onFailure(call: Call<ChartData>, t: Throwable) {
                        callback.showChartFail() // Обработка ошибки
                    }
                })
        }
    }
}