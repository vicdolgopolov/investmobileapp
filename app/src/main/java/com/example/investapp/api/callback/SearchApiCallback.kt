package com.example.investapp.api.callback

import com.example.investapp.pojo.StockPojo

interface SearchApiCallback {
    fun showData(results:ArrayList<StockPojo>?)
    fun showFail(isApiError: Boolean)
}