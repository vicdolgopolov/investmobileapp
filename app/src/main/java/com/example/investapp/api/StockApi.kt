package com.example.investapp.api


import com.example.investapp.pojo.CompanyInfo
import com.example.investapp.pojo.StockPojoData
import com.example.investapp.pojo.chart.ChartData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

// Определяем интерфейс StockApi для работы с API акций
interface StockApi {

    // Метод для получения информации о компании по тикеру
    @GET("overview")
    fun getCompanyInfo(
        @Query("ticker") symbol: String // Параметр запроса, представляющий тикер компании
    ): Call<CompanyInfo> // Возвращаем объект Call с данными CompanyInfo

    // Метод для поиска акций по ключевым словам
    @GET("search")
    fun search(
        @Query("ticker") keywords: String // Параметр запроса, представляющий ключевые слова для поиска
    ): Call<StockPojoData> // Возвращаем объект Call с данными StockPojoData

    // Метод для получения исторических данных по тикеру
    @GET("chart")
    fun getHistory(
        @Query("ticker") ticker: String
    ): Call<ChartData> // Возвращаем объект Call с данными ChartData
}