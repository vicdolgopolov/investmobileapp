package com.example.investapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.investapp.databinding.HolderValueBinding
import com.example.investapp.pojo.OverviewValue
import com.example.investapp.ui.holder.HolderValue

// Класс AdapterValue наследует RecyclerView.Adapter для отображения параметров конкретной акции
class AdapterValue(val values: ArrayList<OverviewValue>) : RecyclerView.Adapter<HolderValue>() {

    // Метод для добавления нового элемента в начало списка
    fun addItem(key: String, value: String) {
        values.add(0, OverviewValue(key, value))
        // Уведомляем адаптер о том, что элемент был добавлен
        notifyItemInserted(0)
    }

    // Метод для создания нового ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderValue {
        // Создаем и возвращаем новый экземпляр HolderValue с привязкой разметки
        return HolderValue(HolderValueBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return values.size
    }

    // Метод для привязки данных к ViewHolder
    override fun onBindViewHolder(holder: HolderValue, position: Int) {
        // Устанавливаем данные для текущего элемента в ViewHolder
        holder.set(values[position])
    }
}