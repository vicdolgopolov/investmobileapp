package com.example.investapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.investapp.databinding.HolderTopStockBinding
import com.example.investapp.pojo.StockPojo
import com.example.investapp.ui.holder.HolderTopStock

// Класс адаптера для отображения списка топовых акций
class AdapterStock(val data:ArrayList<StockPojo>): RecyclerView.Adapter<HolderTopStock>() {

    // Метод для создания нового ViewHolder при необходимости
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderTopStock {
        // Создаем и возвращаем новый экземпляр HolderTopStock с привязкой к макету
        return HolderTopStock(HolderTopStockBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    // Метод для привязки данных к ViewHolder
    override fun onBindViewHolder(holder: HolderTopStock, position: Int) {
        // Устанавливаем данные акции для текущего ViewHolder
        holder.setStock(data[position])
    }


}