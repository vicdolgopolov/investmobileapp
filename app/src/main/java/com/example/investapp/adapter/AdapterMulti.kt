package com.example.investapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.investapp.databinding.HolderFavStockBinding
import com.example.investapp.databinding.HolderMultiValueBinding
import com.example.investapp.pojo.MultiValue
import com.example.investapp.pojo.StockPojo
import com.example.investapp.ui.holder.HolderFavStock
import com.example.investapp.ui.holder.HolderMulti


// Класс AdapterMulti для отображения мультипликаторов
class AdapterMulti(var data: ArrayList<MultiValue>) : RecyclerView.Adapter<HolderMulti>() {

    // Метод для создания нового элемента представления
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderMulti {
        // Возвращаем новый экземпляр HolderMulti с привязкой макета
        return HolderMulti(HolderMultiValueBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    // Метод для привязки данных к элементу представления
    override fun onBindViewHolder(holder: HolderMulti, position: Int) {
        // Устанавливаем данные для текущего элемента представления
        holder.set(data[position])
    }
}