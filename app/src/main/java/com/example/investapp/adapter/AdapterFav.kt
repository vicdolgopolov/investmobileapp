package com.example.investapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.investapp.databinding.HolderFavStockBinding
import com.example.investapp.pojo.StockPojo
import com.example.investapp.ui.holder.HolderFavStock


// Класс адаптера для привязки списка избранных акций к представлению
class AdapterFav(var data: ArrayList<StockPojo>) : RecyclerView.Adapter<HolderFavStock>() {

    // Конструктор по умолчанию, инициализирующий пустой список акций
    constructor() : this(ArrayList<StockPojo>()) {
    }

    fun updateList(data: ArrayList<StockPojo>) {
        this.data = data // Обновляем данные адаптера
        notifyDataSetChanged() // Уведомляем адаптер о том, что данные изменились
    }

    // Метод для создания нового элемента представления
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderFavStock {
        // Возвращаем новый экземпляр HolderFavStock с привязкой макета
        return HolderFavStock(HolderFavStockBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return data.size // Возвращаем размер списка акций
    }

    // Метод для привязки данных к элементу представления
    override fun onBindViewHolder(holder: HolderFavStock, position: Int) {
        holder.setStock(data[position]) // Устанавливаем данные акции в элемент представления
    }
}